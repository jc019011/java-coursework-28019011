package drone_simulation;

public class Drone {

	//Set up the attributes for the class
	public static int drone_identifier = 0;
	private Integer x_pos;
	private Integer y_pos;
	private Integer specific_drone;
	private Direction current_direction;
	
	//Constructor for the class
	Drone(int x, int y, Direction dir){
		y_pos = Integer.valueOf(y);
		x_pos = Integer.valueOf(x);
		current_direction = dir;
		specific_drone = drone_identifier;
		drone_identifier++;
	}
	
	//Check if the Drone is in a certain spot
	public boolean isHere(int x, int y) {
		if(x_pos == x && y_pos == y) {
			return true;
		} else {
			return false;
		}
	}
	
	//try to move the drone to a certain position
	public void tryToMove(DroneArena a) {
		switch(current_direction) {
		case NORTH:
			//if this returns true we move the drone
			if(a.canMoveHere(x_pos, y_pos-1)) {
				//Update the drone position in the arena
				a.moveDrone(this, x_pos, y_pos-1);
				//Update the local drone position
				y_pos--;
			}else {
				current_direction = current_direction.next();
			}
			break;
		case EAST:
			if(a.canMoveHere(x_pos+1, y_pos)) {
				a.moveDrone(this, x_pos+1, y_pos);
				x_pos++;
			}else {
				current_direction = current_direction.next();
			}
			break;
		case SOUTH:
			if(a.canMoveHere(x_pos, y_pos+1)) {
				a.moveDrone(this, x_pos, y_pos+1);
				y_pos++;
			}else {
				current_direction = current_direction.next();
			}
			break;
		case WEST:
			if(a.canMoveHere(x_pos-1, y_pos)) {
				a.moveDrone(this, x_pos-1, y_pos);
				x_pos--;
			}else {
				current_direction = current_direction.next();
			}
			break;
		}
	}
	
	//writes a string of the drone information
	public String writeString() {
		String res = ("Drone "+ specific_drone + " is at " + x_pos.toString() + ", " + y_pos.toString() + ". The drone is moving: "+current_direction.toString());
		return res;
	}
	
	//Calls the show it canvas method
	public void displayDrone(ConsoleCanvas c) {
		c.showIt(x_pos,y_pos,'D');
	}
	
	//getters for the attributes
	public int getXPos() {
		return x_pos;
	}
	public int getYPos() {
		return y_pos;
	}
	public int getCurrentDrone() {
		return specific_drone;
	}
	public Direction getDirection() {
		return current_direction;
	}
	public void setSpecificDrone(int current_drone) {
		specific_drone = current_drone;
	}
}
