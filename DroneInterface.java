package drone_simulation;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;



public class DroneInterface {
	
		//attributes for the droneinterface class
		private Scanner s;							
	   	private DroneArena myArena;	
	   	private String fileName;
	   	String str;
	   	
	   	//constructor for the DroneInterface class
	    public DroneInterface() {
	    	s = new Scanner(System.in);			
	    	myArena = new DroneArena(20, 6);
	    	File directory;
	        char ch = ' ';
	        do {
	        	//Case switch for the menu options
	        	System.out.print("Enter (A)dd drone, get (I)nformation, (D)isplay drones, (M)ove all drones, a(N)imate the drones, create new (B)uilding, (S)ave data, (L)oad data, or e(X)it > ");
	        	ch = s.next().charAt(0);
	        	s.nextLine();
	        	switch (ch) {
	    			case 'A' :
	    			case 'a' :
	        			myArena.addDrone();
	        			break;
	        		case 'I' :
	        		case 'i' :
	        			this.writeInfo();
	            		break;
	        		case 'D' :
	        		case 'd' :
	        			this.doDisplay();
	        			break;
	        		case 'M':
	        		case 'm':
	        			myArena.moveAllDrones();
	        			this.writeInfo();
	        			this.doDisplay();
	        			break;
	        		case 'N':
	        		case 'n':
	        			this.animateDrones();
	        			break;
	        		case 'B':
	        		case 'b':
	        			//Creates a new building
	        			System.out.print("Enter the new x size >");
	        			int x_size = s.nextInt();
	        			System.out.print("Enter the new y size >");
	        			int y_size = s.nextInt();
	        			myArena = new DroneArena(x_size, y_size);
	        			break;
	        		case 'S':
	        		case 's':
	        			//Saves a file Creates a new file first then saves a file
	        			directory = new File("C:/Users/mail_wqlreyr/eclipse-workspace/Part2Java/src/drone_simulation");
	        			System.out.print("Create name of save file > ");
						fileName = s.nextLine();
						String filenames[] = directory.list();
						if (directory.length() != 0)
						{
							for (int i = 0; i < filenames.length; i++) {
								if(fileName == filenames[i])
								{
									fileName = fileName + "(" + i + ")";
								}
							}
						}
						System.out.print("name of file = " + fileName + "\n");
						File outFile = new File("C:/Users/mail_wqlreyr/eclipse-workspace/Part2Java/src/drone_simulation" + fileName + ".txt");
						try {
							FileWriter outFileWriter = new FileWriter (outFile);
							PrintWriter writer = new PrintWriter(outFileWriter);
							writer.println(myArena.getX());
							writer.println(myArena.getY());
							for(Drone d : myArena.getDroneList()) {
								if(d != null) {
									writer.println(d.getCurrentDrone());
									writer.println(d.getXPos());
									writer.println(d.getYPos());
									writer.println(d.getDirection());
								}
							}
							writer.close();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
						
						break;
	        		case 'L':
	        		case 'l':
	        			//Loads the file
	        			System.out.print("Enter name of save file to load > ");
						fileName = s.nextLine();
						String file = fileName + ".txt";
						System.out.print("name of save file = " + fileName + "\n");
						try {
							
							int droneX,droneY,droneVersion;
						    Path pathToFile = Paths.get("C:/Users/mail_wqlreyr/eclipse-workspace/Part2Java/src/drone_simulation" + file);
						    System.out.println(pathToFile.toAbsolutePath());
					        long lineCount = Files.lines(pathToFile).count();
							int a = Integer.parseInt(Files.readAllLines(pathToFile).get(0),10);
							int b = Integer.parseInt(Files.readAllLines(pathToFile).get(1),10);
							
							myArena = new DroneArena(a,b);
							for(int i = 2; i < lineCount; i=i+4)
							{

						        droneVersion = Integer.parseInt(Files.readAllLines(pathToFile).get(i),10);
						        droneX = Integer.parseInt(Files.readAllLines(pathToFile).get(i+1),10);
						        droneY = Integer.parseInt(Files.readAllLines(pathToFile).get(i+2),10);
						        str = Files.readAllLines(pathToFile).get(i+3);
								
								myArena.addPosDrone(droneVersion, droneX, droneY, str);
								
							}
							doDisplay();
						} catch (IOException e) 
						{
						e.printStackTrace();
						}

	        			break;
	        		case 'x' : 	ch = 'X';				
	        			break;
	        		
	        	}
	    		} while (ch != 'X');						
	       s.close();									
	    }
	    
	    //Animate the drones
	    private void animateDrones() {
	    	int count = 10;
	    	while(count>0) {
				
				myArena.moveAllDrones();
    			this.writeInfo();
    			this.doDisplay();
    			count--;
    			try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
	    }
	    
	    //Writes the info for the drones
	    private void writeInfo() {
	    	String res = "";
			for(int i = 0; i < DroneArena.number_of_drones; i++ ) {
				res += myArena.writeString(i);
			}
			System.out.print(res);
	    }
	    
	    //displays the canvas
	    public void doDisplay() {
	    	Integer y_size = myArena.getY();
	    	Integer x_size = myArena.getX();
	    	ConsoleCanvas c = new ConsoleCanvas(x_size,y_size);
	    	myArena.showDrones(c);
	    	System.out.print(c.writeCanvas());
	    }
	    
	//Main function for the package
	public static void main(String[] args) {
		DroneInterface r = new DroneInterface();
	}

}
