package drone_simulation;

import java.util.Random;

public class DroneArena {

	//Set up the attributes for the class
	private Integer x_size;
	private Integer y_size;
	private Drone[][] arena;
	private Random randomGenerator;
	public Drone[] drone_list;
	public static Integer number_of_drones = 0;
	private Drone posDrone;
	
	//Constructor for dronearena
	DroneArena(int x, int y){
		y_size = Integer.valueOf(y);
		x_size = Integer.valueOf(x);
		arena = new Drone[x][y];
		randomGenerator = new Random();
		drone_list = new Drone[x*y];
		
	}
	
	//adds a drone with pre-known values
	public void addPosDrone(int current_drone, int x_pos, int y_pos, String str) {
		
		
		//Sets the different directions
		if(str.equals("NORTH"))
		{
			
			 posDrone = new Drone(x_pos,y_pos,Direction.NORTH);
		}
		if(str.equals("EAST"))
		{
			 
			 posDrone = new Drone(x_pos,y_pos,Direction.EAST);
		}
		if(str.equals("SOUTH"))
		{
		    
			 posDrone = new Drone(x_pos,y_pos,Direction.SOUTH);
		}
		if(str.equals("WEST"))
		{
			 
			 posDrone = new Drone(x_pos,y_pos,Direction.WEST);

		}
		
		//sets the specific drone with the current drone id
		posDrone.setSpecificDrone(current_drone);
		//adds the drone onto the end of the drone list
		int number_of_drones = 0;
		for(Drone d : drone_list) {
			if(d != null) {
				number_of_drones++;
			}
		}
		//Adds the drone into the next null space in the drone list
		drone_list[number_of_drones+1] = posDrone;

	}
	//Adds a drone the arena
	public void addDrone() {
		Integer random_y = null;
		Integer random_x = null;
		//While there is a drone and the random positions are out of bounds
		do {
			random_y = randomGenerator.nextInt(y_size);
			random_x = randomGenerator.nextInt(x_size);
		} while(getDroneAt(random_x, random_y) instanceof Drone || random_y <= 1 || random_x <= 1);
		
		//create a new drone at the random positions
		Drone d = new Drone(random_x, random_y, Direction.getRandomDirection());
		//add drome at number of drones in the arena list
		drone_list[number_of_drones]= d;
		number_of_drones++;
		//add the drone into the arena array
		arena[random_x][random_y] = d;
	}	
	
	//getter for the drone list
	public Drone[] getDroneList() {
		return drone_list;
	}
	
	//gets a drone at two positions
	public Drone getDroneAt(int x, int y) {
		if (arena[x][y] instanceof Drone) {
			return arena[x][y];
		} else {
			return null;
		}
	}
	
	//Writes the current drone data
	public String writeString(Integer drone_number) {
		String res = "";
		res += drone_list[drone_number].writeString();
		res += "\n";
		return res;
	}
	
	//Shows the drones in the canvas
	public void showDrones(ConsoleCanvas c) {
		for(Drone d : drone_list){
			if(d != null) {
				d.displayDrone(c);
			}
		}
	}
	
	//getter for x size
	public Integer getX() {
		return x_size;
	}
	
	//getter for y size
	public Integer getY() {
		return y_size;
	}
	
	//checks if a drone can move to a specific point
	public boolean canMoveHere(Integer x_pos, Integer y_pos) {
		if(x_pos >= x_size-1 || x_pos<=0) {
			return false;
		}else if(y_pos>=y_size-1 || y_pos<=0){
			return false;
		}else if(getDroneAt(x_pos, y_pos) instanceof Drone) {
			return false;
		}else {
			return true;
		}
	}
	
	//moves all the drones
	public void moveAllDrones() {
		for(Drone d : drone_list) {
			if(d != null) {
				d.tryToMove(this);
			}
			
		}
	}
	
	//moves a single drone
	public void moveDrone(Drone d, Integer new_x, Integer new_y) {
		arena[d.getXPos()][d.getYPos()] = null;
		arena[new_x][new_y]=d;
	}
	
}
