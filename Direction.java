package drone_simulation;
import java.util.Random;

public enum Direction {
	//The directions for the enums
	NORTH,
	EAST,
	SOUTH,
	WEST;
	
	//A list of the values
	private static Direction[] vals = values();
	
	//Gets a random direction
	public static Direction getRandomDirection() {
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}
	//returns the next direction in N,E,S,W
	public Direction next() {
		return vals[(this.ordinal()+1) % vals.length];
	}
}
